package com.kpr.training.string;
/*
Requirement:
    To find length of the given String and find the character value at 12 th position and also find the letter b in which position.
    String hannah = "Did Hannah see bees? Hannah did.";

Entities:
    There is no entity.
       
Function Signature:
    No, function declared.
    
Jobs to be done:
    1)Find the length of the given string.
    2)Find the character value at 12th position from the given String.
    3)Find the letter "b" in which position from the given String.

Solution:
    1)32
    2)e
    3)hannah.charAt(15)
 */

public class Hannah {

	public static void main(String[] args) {
		String hannah = "Did Hannah see bees? Hannah did.";
		System.out.println(hannah.length());
		System.out.println(hannah.charAt(12));
		System.out.println(hannah.charAt(15));
	}
}

/*
 * Solution: 1)32 2)e 3)hannah.charAt(15)
 */
