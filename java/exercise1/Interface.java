package test;

public interface Interface {
	public void printName();

	private void printAge(); // shows error because interface does not permit private and protected
								// access modifiers

	protected void printNationality(); // shows error because interface does not permit private and
										// protected access modifiers
}
