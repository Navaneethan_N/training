package test;

class DifferentClass {

	public static String name;
	private static int age;
	protected static String nationality;
}


public class InDifferentClass {

	public static void main(String[] args) {

		DifferentClass otherClass = new DifferentClass();

		otherClass.name = "Balaji";
		System.out.println(otherClass.name);

		otherClass.age = 20; // Shows error because age has private access modifier
		System.out.println(otherClass.age);

		otherClass.nationality = "Indian";
		System.out.println(otherClass.nationality);
	}

}
