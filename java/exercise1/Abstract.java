package test;

public abstract class Abstract {

	public abstract void printName();

	private abstract void printAge(); // shows error because other classes which extends Abstract
										// class cannot access this method.

	protected abstract void printNationality();
}
