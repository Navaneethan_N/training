/*
 * Requirements : 
 * 		To explain about contains(), subList(), retainAll() and give example.
 * 
 * Entities :
 * 		ListExample.
 * 
 * Method Signature :
 * 		public static void main(String[] args)
 * 
 * Jobs To Be Done:
 *      1) Create an object for ArrayList with type as Integer.
 *      2) Add the elements to the list.
 *      3) Check Whether a specific element is present in the list.
 *      	3.1) If present, Print true.
 *          3.2) Otherwise, Print false.
 *      4) Print the range of the element in the list.
 *      5) Create an object for new ArrayList with type as Integer and Add some other elements to the new list.
 *      6) Print the elements of new list.
 *      7) Using retainAll method for the new list.
 *      8) Print the new list elements.
 *      
 * Pseudo Code:
 *
 *class ListExample{
 *     public static void main(String[] args) {
 *          ArrayList<Integer> list = new ArrayList<>();
 *          //Add the elements to the list
 *          System.out.println(list.contains(element));
 *          System.out.println(list.subList(start , end));
 *          ArrayList<Integer> newList = new ArrayList<>();
 *          //Add elements to newList
 *         System.out.println("Before using retainall method : " + newList);
 *         newList.retainAll(list);
 *          System.out.println("After using retainall method : " + newList);
 *    }   
 *
 *}
 *
 * 	    
 */
/*
 * Explanation:
 * 	contains():
 * 		returns true if the element is present in the list else returns false.
 * 	subList(starting index, ending index):
 * 		returns the list elements from starting index to ending index.
 * 	retainAll():
 * 		It is used to remove all the array list's elements that are not contained in the specified list.
 */
package com.kpr.training.list_and_set;

import java.util.ArrayList;

public class ListExample {

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		list.add(5);
		list.add(6);
		System.out.println(list.contains(5)); // it prints true
		System.out.println(list.subList(1, 5)); // prints elements from index 1 to 5

		ArrayList<Integer> newList = new ArrayList<>();
		newList.add(3);
		newList.add(4);
		newList.add(10);
		System.out.println("Before using retainall method : " + newList);
		newList.retainAll(list);
		System.out.println("After using retainall method : " + newList);
	}

}
