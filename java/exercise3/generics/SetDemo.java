/*
 * Requirements : 
 *   To write a program to demonstrate generics - for loop for set.
 *
 * Entities :
 *    SetDemo 
 *    
 * Method Signature :
 *    	public static void main(String[] args)
 *    
 * Jobs To Be Done:
 *     1)Create object for HashSet with type as String.
 *     2)Add some names to the HashSet.
 *     3)for each element in the HashSet.
 *     		3.1)Print the element.
 *     
 * PseudoCode:
 * 		class SetDemo {

		    public static void main(String[] args) {
		        HashSet<String> names = new HashSet<String>();
		        //Add some names
		        System.out.println("Iterating String set using while loop");
		        
		        //Print using iterator.
		        Iterator<String> iterator = names.iterator();
		        while (iterator.hasNext()) {
		            System.out.println(iterator.next());
		        }
				
				//print using for each loop.
		        System.out.println("Iterating String set using for loop");
		        for (String name : names) {
		            System.out.println(name);
		        }
		    }
		}
 */
package com.kpr.training.generics;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;

public class SetDemo {

    public static void main(String[] args) {
        HashSet<String> names = new HashSet<String>();
        String firstName = "Balaji";
        names.add(firstName);
        names.add("Arya");
        names.add("Surya");
        names.add("Ajith");
        names.add("Ajith");
        System.out.println("Iterating String set using while loop");
        Iterator<String> iterator = names.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        System.out.println("Iterating String set using for loop");
        for (String name : names) {
            System.out.println(name);
        }
    }
}
