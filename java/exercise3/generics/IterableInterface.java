/*Requirements:
 * 		To write a program to print employees name list by implementing iterable interface.
 * 
 * Entity:
 * 		MyIterable, IterableInterface
 * 
 * Method Signature:
 * 		public MyIterable(T[] t),
 * 		public Iterator<T> iterator().
 * 
 * Jobs To Be Done:
 * 		1)Create a list and add some names to it.
 * 		2)Create a reference for IterableInterface interface with list as constructor parameter.
 * 		3)for each name in the list.
 * 			3.1)print the name.
 * 
 * Pseudo Code:
 * 		public class IterableInterface<T> implements Iterable<T> {
 *	
 *			//declare a local variable called as list
 * 
 * 			//declare a method declaration that adds the employee name to the local variable list.
 *
 *	 		public Iterator<T> iterator() {
 *				return list.iterator();
 * 			//return the list that has been iterated.
 * 			}
 * 
 * 			public static void main(String[] args) {
 * 				List<String> employeeNameList = Arrays.asList("Balaji Pa", "Boobalan P", "Gokul D", "Karthikeyan C", "Kiruthic P");
 *				//calling the MyIterable method to print it in Iterable interface.
 *				IterableInterface<String> myList = new IterableInterface<>(employeeNameList);
 *				//printing the employee name that is iterated and returned from iterator method.	
 *				for (String i : myList) {	
 *					System.out.println(i);	
 *				}
 *			}
 *		}
 */	

package com.kpr.training.generics;

import java.util.List;
import java.util.Arrays;
import java.util.Iterator;

public class IterableInterface<T> implements Iterable<T> {
	
	private List<T> list;

	public IterableInterface(List<T> employeeName) {

		this.list = employeeName;

	}

	public Iterator<T> iterator() {
		return list.iterator();
	}

	public static void main(String[] args) {

		List<String> employeeNameList = Arrays.asList("Balaji Pa", "Boobalan P", "Gokul D", "Karthikeyan C", "Kiruthic P");
		IterableInterface<String> myList = new IterableInterface<>(employeeNameList);

		for (String i : myList) {
			System.out.println(i);
		}
	}
}

