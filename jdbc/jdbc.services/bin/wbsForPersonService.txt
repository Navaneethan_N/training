WBS for CRUD operations in Person Service

WBS for Create method:

/*
Requirement:
    To create person details like person id, email, address_id , birth_date and created_date in person table
    
Entity:
    1.Person
    2.PersonService
    3.AppException
    
Function declaration:
    public long create(Person person, Address address)
    
Jobs to be done:
    1. Declare the noOfRowsAffected and personId as Zero.
    2. Create an instance for AddressService class using addressService object
    3. Create an instance jc and get connection with the database using jdbc driver by using init()
    4. Declare PreparedStatement as null
    5. Check the email is unique 
	     5.1.1) If unique then
	     5.1.2) Invoke the  AddressService.create 
	     5.1.3) And return the id of address table to the variable id. 
	     5.1.4) If not unique throw new AppException "email not unique".
    6.Inside try  block pass the query from QueryStatement 
      as the parameter to PreparedStatement and store it in ps.
    7.With the connection ps , Setting the column inputs for name, email and birth_date.
        7.1)Store the number of Rows Affected by using executeUpdate()
        7.2)store it in noOfRowsAffected.
        7.3)Get the generated keys and store it in person id  
        7.4)using loop iterate each personId, 
        7.5)store each person id to the variable to the personId and close the ps.
        7.6)Check whether the noOfRowsAffected is zero.
    		 7.6.1)If zero , No data is created. So throw AppException with the message "Person creation Fails"
             7.6.2)If not Zero save the connection service using commit()
    8.If any exception catches it rollback and throw Exception 
    9.Close the connection with the database and return the personId.
  
Pseudo Code:

class PersonService {

	public long create(Person person, Address address) {
		
		long noOfRowsAffected = 0;
		long personID = 0;
		AddressService addressService = new AddressService();
		
		if (address.getPostalCode() == 0) {
			//throw new AppException
		} else {
			ConnectionService jc = new ConnectionService();
			jc.init();
			
			PreparedStatement ps = null;

			if(isUnique(person.getEmail(), ps, jc.con) == true) {
				long id = addressService.create(jc.con, address);
				
				try {
					
					ps = jc.con.prepareStatement(QueryStatement.createPersonQuery.toString(),
							ps.RETURN_GENERATED_KEYS);
					ps.setString(1, person.getName());
					ps.setString(2, person.getEmail());
					ps.setDate(3, person.getBirthDate());
					ps.setLong(4, id);
					noOfRowsAffected = ps.executeUpdate();
					ResultSet personId = ps.getGeneratedKeys();
					
					while (personId.next()) {
						personID = personId.getLong("GENERATED_KEY");
					}
					ps.close();
					
					if (noOfRowsAffected == 0) {
						//throw new AppException
					}
					
					jc.con.commit();

				} catch (SQLException e) {
					try {
						jc.con.rollback();
					} catch (SQLException ex) {
						//throw new AppException
					}
					//throw new AppException
				}
			} else {
				//throw new AppException
			}
			
			jc.release();
		}
		
		return personID;
	}

*/

WBS For read method:
/*
Requirement:
    To read person details like person id, email, address_id , birth_date and created_date from person table
    
Entity:
    1.Person
    2.PersonService
    3.AppException
    
Function declaration:
    public Person read(long id, boolean addressFlag)

Jobs To Be Done:
    1.Create person class as a type Person and declare null.
    2.Create an instance for AddressService , ConnectionService and using connection Service object, invoke init().
    3.Inside try block 
        3.1) pass the query from QueryStatement 
             as the parameter to PreparedStatement and store it in ps.
        3.2)Set the id which you want to read 
        3.3)Execute the query and store it in ResultSet.
    7.For each value in result set,
	    7.1) Get the name ,email and birthDate ,createdDate and id
             from ResultSet Class and pass it as a parameter to Person class
        7.2) Check if the addressFlag is given then read address by getting id
             by invoking addressService.read method.
        7.3) Close the ps connection.
    8.If any Exception catches throw AppException 
        8.1) close the connection using release()
    9.Return person
    
   
Pseudo Code:

class PersonService {

    public Person read(long id, boolean addressFlag) {
		
        Person person = null;
        AddressService addressService = new AddressService();
        ConnectionService jc = new ConnectionService();
        jc.init();

        try {
            
        	PreparedStatement ps;
            ps = jc.con.prepareStatement(QueryStatement.readPersonQuery.toString());
            ps.setLong(1, id);
            ResultSet result = ps.executeQuery();

            while (result.next()) {
                person = new Person(result.getString("name"), result.getString("email"), result.getDate("birth_date"));
                person.setCreatedDate(result.getTimestamp("created_date"));
                person.setId(id);
                if (addressFlag) {
                	person.setAddress(addressService.read(jc.con, result.getLong("address_id")));
                }
            ps.close();
            
            }
        } catch (SQLException e) {
            throw new AppException(ErrorCode.SQLException);
        }
        
        jc.release();
        return person;
    }


*/

WBS for readall method:
/*
Requirement:
    To read all the person details like person id, email, address_id , birth_date and created_date from person table
    
Entity:
    1.Person
    2.PersonService
    3.AppException
    
Function declaration:
    public ArrayList<Person> readAll()
 
Jobs to be Done:
	1. Create a ArrayList as Persons as type Person.
    2. Create an instance for AddressService , ConnectionService and using connection Service object, invoke init().
    3.Inside try block 
        3.1) pass the query from QueryStatement 
             as the parameter to PreparedStatement and store it in ps.
    4.Execute the query statement and the executed query is assigned to the result set.
	5.For each value in result set,
       6.1) Get the name ,email and birthDate from ResultSet Class and pass it as a parameter to Person 
	   6.2) Read the AddressId and store it in address
	   6.3) Set the Created date in Person 
       6.4) set the address in person
	   6.5) Add the person to the persons.
    7.Close the ps connection.
    8.If any Exception catches throw AppException 
        8.1) close the connection using release()
    9.Return person
    
	
    
Pseudo Code:

class PersonService {

    public ArrayList<Person> readAll() {
	    
        ArrayList<Person> persons = new ArrayList<>(); 
        
        ConnectionService jc = new ConnectionService();
        jc.init();
        AddressService addressService = new AddressService();

        try {
            
            PreparedStatement ps;
            ps = jc.con.prepareStatement(QueryStatement.readAllPersonQuery.toString());
            ResultSet result = ps.executeQuery();

            while (result.next()) {
                Person person = new Person(result.getString("name"), result.getString("email"),
                        result.getDate("birth_date"));
                Address address = addressService.read(jc.con, result.getLong("address_id"));
                person.setCreatedDate(result.getTimestamp("created_date"));
                person.setAddress(address);
                persons.add(person);
            }
            ps.close();
            
        } catch (SQLException e) {
            throw new AppException(ErrorCode.SQLException);
        }

        jc.release();
        return persons;
    }

WBS For update method:

/*
Requirement:
    To update person details like person id, email, address_id , birth_date and created_date from person table
    
Entity:
    1.Person
    2.PersonService
    3.AppException
    
Function declaration:
    public void update(long id, Person person, Address address)

Jobs To Be Done:
    1. Declare the variable numberOfRowsAffected as zero 
    2. Checking whether the PostalCode is zero
        2.1) Throw an AppException with message "postal code should not be zero".
    3. Checking whether the id is zero
        3.1) Then throw an AppException with message "Specified person id does not exist".
    4.Otherwise
      4.1) Create an instance jdbcConnection and get connection with the database using jdbc driver.
      4.2) Invoke the init method from ConnectionService using the object jc.
      4.2) Check whether the email is unique
           4.2.1)Store the updateAddress query from the QueryStatements in the preparedStatement of jc. 
           4.2.2)Invoke the method getStreet from Address and set the value for street.
           4.2.3)Invoke the method getCity from Address and set the value for city.
           4.2.4)Invoke the method getPostalCode from Address and set the value for posal code.. 
           4.2.5)Invoke the method getName from Person and set the value for name.          
           4.2.6)Invoke the method getEmail from Person and set the value for email.
           4.2.7)Invoke the method getBirthDate from Person and set the value for Birthdate.
           4.2.8)Set the value for person id as id.
           4.2.9)Exceute the query in the prepared statement and store it in the numberOfRowsAffected of type long.
           4.2.10) If any exception occurs during updation then throw an exception with the message "SQL exception occured".
           4.2.11) Check whether the numberOfRowsAffected is zero 
                 4.2.11.1)  Then throw AppException with message "updation failed".
                 4.2.11.2)  Otherwise Closing the Jdbc driver connection.
    6. If the email is not unique throw AppException with message "Email already exist".

    
Pseudo Code:

    public void updatePerson(long id, Person person, Address address) {

        int numberOfRowsAffected = 0;
        
        if (address.getPostalCode() == 0) {
            throw new AppException(ExceptionCode.POSTAL_CODE_ZERO);
        } else {
            ConnectionService jc = new ConnectionService();
            jc.init();
            PreparedStatement ps = null;

            if(isUniqueExceptID(id, person.getEmail(), ps, jc.connection) == true) {

                try {
                    ps = jc.connection.prepareStatement(QueryStatements.updatePersonQuery.toString());
                    ps.setString(1, address.getStreet());
                    ps.setString(2, address.getCity());
                    ps.setInt(3, address.getPostalCode());
                    ps.setString(4, person.getName());
                    ps.setString(5, person.getEmail());
                    ps.setDate(6, person.getBirthDate());
                    ps.setLong(7, id);
                    numberOfRowsAffected = ps.executeUpdate();
                } catch (SQLException e) {
                    throw new AppException(ExceptionCode.SQL_EXCEPTION);
                }
                if (numberOfRowsAffected == 0) {
                    throw new AppException(ExceptionCode.PERSON_UPDATION_FAILED);
                } else {
                    jc.release();
                }
            } else {
                throw new AppException(ExceptionCode.EMAIL_NOT_UNIQUE);
            }
        }
    }

*/

WBS For delete method:
/*
Requirement:
    To delete person details like person id, email, address_id , birth_date and created_date from person table
    
Entity:
    1.Person
    2.AppException
    
Function declaration:
    public void delete(long id)

Jobs To Be Done:
    1. Declare numberOfRowsAffected as zero.
    2. Create an object for CreateService as jc. 
    3. Declare the PreparedStatement as ps.
    4. Declare the deleteAddressQuery from QueryStatement as String to ps.
    5. Set the id of the address to be deleted.
    6. Execute the PreparedStatement and store the value in numberOfRowsAffected.
    7. Close the PreparedStatement.
    8. If any exception occurs during deletion then thrown an exception with the message "SQL exception occured".   
    9. Check whether the numberOfRowsAffected is zero
       9.1. Throw a exception with the message "person deletion failed".
       9.2. Othewise invoke the release method from ConnectionService using the object jc.
    
Pseudo Code:
    public void delete(long id) {

        int numberOfRowsAffected = 0;
        ConnectionService jc = new ConnectionService();
        jc.init();
        
        try {
            
            PreparedStatement ps;
            ps = jc.connection.prepareStatement(QueryStatements.deletePersonQuery.toString());
            ps.setLong(1, id);
            numberOfRowsAffected = ps.executeUpdate();
            ps.close();
            
        } catch (SQLException e) {
            throw new AppException(ExceptionCode.SQL_EXCEPTION);
        }
        if (numberOfRowsAffected == 0) {
            throw new AppException(ExceptionCode.PERSON_DELETION_FAILED);
        } else {
            jc.release();
        }
    }
       */