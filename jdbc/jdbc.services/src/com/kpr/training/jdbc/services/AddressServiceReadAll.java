package com.kpr.training.jdbc.services;

/*
Requirement :
    To read all the data in the address 

Entity:
    AddressServiceReadAll

Function Declaration :
    public static void readAll()

Jobs To Be Done :
    1. Create a ArrayList with the name street and type String .
       1.1) Create a ArrayList with the name city and type String .
       1.2) Create a ArrayList with the name postal_code and type integer.
       1.3) Create a ArrayList with the name id and type long.
    2. Establish the connection using jdbc driver from createConnection method in jc.
    3. Prepare readall query and store it in query of type String.
    4. Inside try block 
       4.1) Create a prepareStatement for the query using jc.con as jc.ps.
       4.2) Create instance for ResultSet class as result
       4.3) Using loop read all values like street,city,postal_code ,id
            and store it in result 
    5. If any Error causes catch block catch the error 
    6. Using loop print the readed values
    7. Close the jdbc connection.

Pseudo code :

public class AddressServiceReadAll {

     public static void readAll() {
        ArrayList<String> street = new ArrayList<>();
        ArrayList<String> city = new ArrayList<>();
        ArrayList<Integer> postal_code = new ArrayList<>();
        ArrayList<Long> id = new ArrayList<>();
        JdbcConnection jc = new JdbcConnection();
        jc.createConnection();
        String query = "select * from `jdbc`.address";
        try {
            jc.ps = jc.con.prepareStatement(query);
            ResultSet result = jc.ps.executeQuery(query);
            while (result.next()) {
                street.add(result.getString("street"));
                city.add(result.getString("city"));
                postal_code.add(result.getInt("postal_code"));
                id.add(result.getLong("id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for(int i = 0; i < id.size(); i++) {
            System.out.println(id.get(i) + " " + street.get(i) + " " +  city.get(i) + " " +  postal_code.get(i));
        }
        
        jc.closeConnection();
    }
}
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AddressServiceReadAll {
    
    public static void readAll() {
        ArrayList<String> street = new ArrayList<>();
        ArrayList<String> city = new ArrayList<>();
        ArrayList<Integer> postal_code = new ArrayList<>();
        ArrayList<Long> id = new ArrayList<>();
        JdbcConnection jc = new JdbcConnection();
        jc.createConnection();
        String query = "select * from `jdbc`.address";
        try {
            jc.ps = jc.con.prepareStatement(query);
            ResultSet result = jc.ps.executeQuery(query);
            while (result.next()) {
                street.add(result.getString("street"));
                city.add(result.getString("city"));
                postal_code.add(result.getInt("postal_code"));
                id.add(result.getLong("id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        for(int i = 0; i < id.size(); i++) {
            System.out.println(id.get(i) + " " + street.get(i) + " " +  city.get(i) + " " +  postal_code.get(i));
        }
        
        jc.closeConnection();
    }
}
