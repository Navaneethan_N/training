package com.kpr.training.jdbc.constant;

public class QueryStatement {

    public static final String createAddressQuery =
            "INSERT INTO `jdbc`.address (street, city, postal_code) VALUES (?, ?, ?)";

    public static final StringBuilder readAddressQuery = new StringBuilder(
            "SELECT address.id"
               + ", address.street"
               + ", address.city"
               + ", address.postal_code "
            + "FROM `jdbc`.address " 
           + "WHERE `id` = ?");

    public static final StringBuilder readAllAddressQuery = new StringBuilder(
            "SELECT address.id"
               + ", address.street"
               + ", address.city"
               + ", address.postal_code "
           + "FROM `jdbc`.address");

    public static final StringBuilder updateAddressQuery = new StringBuilder(
            "UPDATE `jdbc`.address "
             + "SET street = ?"
               + ", city = ?"
               + ", postal_code = ? "
           + "WHERE id = ?");

    public static final StringBuilder deleteAddressQuery = new StringBuilder(
            "DELETE FROM `jdbc`.address"
               + " WHERE id=?");

    public static final StringBuilder createPersonQuery = new StringBuilder(
            "INSERT INTO `jdbc`.person (name, email, birth_date, address_id) VALUES (?, ?, ?, ?)");

    public static final StringBuilder readPersonQuery = new StringBuilder(
            "SELECT person.id"
               + ", person.name"
               + ", person.email"
               + ", person.address_id"
               + ", person.birth_date"
               + ", person.created_date "
           + "FROM `jdbc`.person "
          + "WHERE `id` = ?");

    public static final StringBuilder readAllPersonQuery = new StringBuilder(
            "SELECT person.id"
               + ", person.name"
               + ", person.email"
               + ", person.address_id"
               + ", person.birth_date"
               + ", person.created_date "
          + "FROM `jdbc`.person");

    public static final StringBuilder updatePersonQuery = new StringBuilder(
            "UPDATE `jdbc`.person "
            + "LEFT JOIN `jdbc`.address "
              + "ON person.address_id = address.id "
             + "SET address.street = ?"
               + ", address.city = ?"
               + ", address.postal_code = ?"
               + ", person.name = ?"
               + ", person.email = ?"
            + ", person.birth_date = ? "
          + "WHERE person.id = ?");

    public static final StringBuilder deletePersonQuery = new StringBuilder(
           "DELETE `jdbc`.address"
              + ", `jdbc`.person "
           + "FROM `jdbc`.person "
          + "INNER JOIN `jdbc`.address "
             + "ON person.address_id = address.id "
          + "WHERE person.id = ?");

    public static final StringBuilder emailUnique = new StringBuilder(
            "SELECT person.id "
          + "FROM `jdbc`.person "
          + "WHERE person.email = ?");
}
