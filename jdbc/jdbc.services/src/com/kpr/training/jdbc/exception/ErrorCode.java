package com.kpr.training.jdbc.exception;

public enum ErrorCode {

	POSTAL_CODE_ZERO("ERR401", "postal code should not be zero")
	,SQLException("ERR402", "SQL exception occurs")
	,ADDRESS_CREATION_FAILS("ERR403", "Address was not created")
	,ADDRESS_UPDATION_FAILS("ERR404", "Failed to update Address")
	,ADDRESS_DELETION_FAILS("ERR405", "Failed to delete Address")
	,PERSON_CREATION_FAILS("ERR406", "Person was not created")
	,PERSON_UPDATION_FAILS("ERR407", "Failed to update Person")
	,PERSON_DELETION_FAILS("ERR408", "Failed to delete Person")
	,IO_EXCEPTION("ERR409", "IO exception occurs")
	,EMAIL_NOT_UNIQUE("ERR411", "Email should be unique");

	private final String code;
	private final String msg;

	ErrorCode(String id, String msg) {
		this.code = id;
		this.msg = msg;
	}

	public String getId() {
		return this.code;
	}

	public String getMsg() {
		return this.msg;
	}
}
